'use strict';

let util = require('util');
let http = require('http');
let Bot  = require('@kikinteractive/kik');
let WyspaStateMachine = require('./WyspaStateMachine/WyspaStateMachine.js')

// Configure the bot API endpoint, details for your bot
let bot = new Bot({
    username: 'pianobot',
    apiKey: 'c71969e5-1f9c-44c4-a85f-924edc874365',
    baseUrl: 'https://wyspabot.herokuapp.com/'
});

let wyspaStateMachine = new WyspaStateMachine(bot);

bot.updateBotConfiguration();

bot.onStartChattingMessage((message) => {
    console.log('message.from: ' + message.from);
    return wyspaStateMachine.processKikMessage(message)
        .catch(
            function(err)
            {
                console.error(err);
            })
});

//Where state
bot.onTextMessage((message) => {
    console.log('message.from: ' + message.from);
    return wyspaStateMachine.processKikMessage(message)
        .catch(
        function(err)
        {
            console.error(err);
        })
});

// Set up your server and start listening
let server = http
    .createServer(bot.incoming())
    .listen(process.env.PORT || 8080);