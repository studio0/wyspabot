"use strict";
var GenericMessenger = require('./GenericMessenger.js');
var request = require('request-promise');

class TestMessenger extends GenericMessenger {
  constructor()
  {
    super();

    this.name = 'Test';
  }

  getUserProfile(userId)
  {
    return Promise.resolve({
      username: 'jsmith',
      fullName: 'John Smith'
    });
  }

  sendMessage(sender, messageData)
  {       
    console.log(JSON.stringify(messageData));
  }
}

module.exports = TestMessenger;