"use strict";
var GenericMessenger = require('./GenericMessenger.js');
var request = require('request-promise');

class FacebookMessenger extends GenericMessenger {
  constructor(token)
  {
    super();
    if (!token)
    {
      throw new Error('No access token provided!');
    }

    this.token = token;
    this.name = 'Facebook';
  }
  
  getUserProfile(userId)
  {
    console.log('FacebookMessenger.getUserProfile: ' + userId);
    var requestUrl = `https://graph.facebook.com/v2.6/${userId}?fields=first_name,last_name,profile_pic&access_token=${this.token}`;
    // console.log(' >FacebookMessenger.getUserProfile: Url is ' + requestUrl);
    return request(requestUrl)
      .then(
        function(body)
        {
          console.log(' >FacebookMessenger.getUserProfile: Got response!');
          return JSON.parse(body);
        });
  }

  sendMessage(sender, messageData)
  {       
    let requestData = {
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token:this.token},
        method: 'POST',
        json: {
          recipient: {id:sender},
          message: messageData,
        }
      };

    // console.log('requestData: ' + JSON.stringify(requestData));

    return request(requestData);
  }
}

// test()
function test()
{
  var fbBot = new FacebookMessenger('CAAMCrxv6CFIBAH6fevBtnyp4dHK0sEUwoc44IGzZBDDK6RNQlCTvqrQnTtUsZBAttPAAjerQxbTl8upcXWkWyFiYbtOzhDsEjnGdZBLA5aZBX23NSsiYyrY7BkzZBEMeH0Gmud7WU3xIJ92Nt48IIoNkj7xfDsYJ70Pb57qrbdJ7Eyb80ySwPpviZB3tpXGXMZD');

  let testUrl = GenericMessenger.createWebUrlElement('www.google.ca', 'Google');
  let testPostbackElement = GenericMessenger.createPostbackElement('Test title', 'Test postback');
  let testCardElement = GenericMessenger.createCardElement('title', 'subtitle', null, [testPostbackElement, testUrl]);
  // console.log('')
  // fbBot.sendCards(1080516395327521, [testCardElement, testCardElement]) 
  fbBot.sendTextMessage(1080516395327521, 'test message') 
  // fbBot.sendButtonMessage(1080516395327521, 'title', [testPostbackElement, testUrl]) 
    .catch(
      function(err)
      {
        console.error('error: ' + JSON.stringify(err, null, 2));
      })
}

module.exports = FacebookMessenger;