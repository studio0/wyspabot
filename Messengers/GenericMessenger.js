"use strict";

var request = require('request-promise');

class GenericMessenger {
  constructor()
  {
    this.name = 'GenericMessenger';
  }

  getUserProfile(userId)
  {
    return new Promise.reject('This must be overridden!');
  }

  //Create
  static createCardElement(title, subtitle, imageUrl, buttons)
  {
    return {
              "title":title,
              "image_url":imageUrl,
              "subtitle":subtitle,
              "buttons":buttons
            }
  }

  static createPostbackElement(title, postback)
  {
    return {
              "type":"postback",
              "title":title,
              "payload": postback || title
            }      
  }

  static createWebUrlElement(url, title)
  {
    return {
              "type":"web_url",
              "url":url,
              "title":title
            }
  }

  //Sending
  sendTextMessage(sender, text) {
    let messageData = {
      text:text
    }

    return this.sendMessage(sender, messageData);
  }

  // sendCardMessages(sender, cardMessages)
  // {
  //   let cardMessageData = cardMessages.map(
  //     function(cardMessage)
  //     {
  //       return this.createCardElement(cardMessage.title, cardMessage.subtitle, cardMessage.imageUrl, cardMessage.suggestions, cardMessage.suggestionPostbacks);
  //     }.bind(this));

  //   let messageData = {
  //     "attachment": {
  //       "type": "template",
  //       "payload": {
  //           "template_type":"generic",
  //           "elements": cardMessageData
  //       }
  //     }
  //   };    

  //   return this.sendMessageToFacebook(sender, messageData);
  // }
  
  sendButtonMessage(sender, title, buttons) {
    let messageData = {
      "attachment": {
        "type": "template",
        "payload": {
  	        "template_type":"button",
  	        "text":title,
  	        "buttons": buttons
  	    }
      }
    };
    
    return this.sendMessage(sender, messageData);
  }

  sendCards(sender, elements)
  {
    let messageData = {
      "attachment": {
        "type": "template",
        "payload": {
            "template_type":"generic",
            "elements": elements
        }
      }
    };

    return this.sendMessage(sender, messageData);
  }

  sendMessage(sender, messageData)
  {       
    return new Promise(
      function(resolve, reject)
      {
        reject('You must override this function');
      })
  }
}

module.exports = GenericMessenger;