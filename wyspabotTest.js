'use strict';
var path = require('path');
var express = require('express');
var app = express();
let WyspaStateMachine = require('./WyspaStateMachine/WyspaStateMachine.js')
let wyspaStateMachine = new WyspaStateMachine();

var TestMessenger = require('./Messengers/TestMessenger.js')
let testBot = new TestMessenger();
wyspaStateMachine.addMessenger(testBot);

var bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//Add client-side bundle
// app.use(express.static(path.join(__dirname, 'build')));

/// catch 404 and forward to error handler
app.post('/chat', function(req, res, next) {
  console.log('req: ' + JSON.stringify(req.body));
  res.sendStatus(200);
  return wyspaStateMachine.processGenericMessage(req.body.message, req.body.userId, 'Test')
    .catch(
      function(err)
      {
        console.error(err);
      });
});

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.error(err);
    // res.render('error', {
    //     message: err.message,
    //     error: {}
    // });
});

app.set('port', process.env.PORT || 7019);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});