"use strict";

var Buffer = require('buffer').Buffer;
var request = require('request-promise');
var util = require('util');

let x_gwt_permutation_default = "1C2AABFE96C23FE92C501581C44594B6";

function addDaysToDate(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

function getMatches(string, regex, index) {
	index || (index = 1); // default to the first capturing group
	var matches = [];
	var match;
	while (match = regex.exec(string)) {
	    matches.push(match[index]);
	}
	return matches;
}

class GoogleFlightsApi {
	constructor()
	{
	}	

	static getPlacesFromName(locationQuery)
	{	
		function generateRequestPayload(locationQuery) 
		{           
	        // json = '[,[[,"ca","[,{\\"1\\":[[,[' + json_start_location+'],[\\"'+endAirportCode+'\\"],\\"\\"],[,[\\"'+endAirportCode+'\\"],['+json_start_location+'],\\"\\"]],\\"17\\":1,\\"18\\":[[,3,1]]},\\"'+startDateString+'\\",\\"'+endDateString+'\\",,[' + numberOfDays + ']]","1877941884419597",9]],[,[[,"b_al","no:69"],[,"b_ahr","no:s"],[,"b_ca","193:13965"],[,"b_pe","51CD454107B21.AB6CA0A.2D059A39"],[,"b_qu","0"],[,"b_qc","1"]]]]';
	        var json = `{"1":[{"1":"aa","2":"{\\"1\\":1,\\"2\\":\\"${locationQuery}\\"}","3":"1706921297429966","4":48}],"2":{"1":[{"1":"b_al","2":"aa:52"},{"1":"b_ahr","2":"aa:s"},{"1":"b_am","2":"aa"},{"1":"b_qu","2":"0"},{"1":"b_qc","2":"1"}]}}`;
	        return json;
	    }

		//Connect to Google and get flight data from it 
		function getLocationsFromQueryFromGoogle(locationQuery)
		{
			console.log("Getting info from Google...");
			//The payload that tells Google API what we info we want. Make sure the escapes are themselves escaped.
			var dataAsString = generateRequestPayload(locationQuery);
			// var dataAsString = '[,[[,"ca","[,{\\"1\\":[[,[\\"HKG\\"],[\\"KIX\\"],\\"\\"],[,[\\"KIX\\"],[\\"HKG\\"],\\"\\"]],\\"17\\":1,\\"18\\":[[,3,1]]},\\"2015-08-08\\",\\"2015-10-06\\",,[5]]","1877941884419597",9]],[,[[,"b_al","no:69"],[,"b_ahr","no:s"],[,"b_ca","193:13965"],[,"b_pe","51CD454107B21.AB6CA0A.2D059A39"],[,"b_qu","0"],[,"b_qc","1"]]]]'
			console.log("Generated payload: " + dataAsString.length.toString() + " bytes");

			//The options for the POST request
			//Note: Setting content-length or content-type cause a 500 response code to return
			var options = {
			    url: "https://www.google.ca/flights/rpc",
			    port: 443,
			    method: 'POST',
			    headers: { 
			        'x-gwt-permutation': 'FE78D3AF81FAD04A45680790F43E47C3',
			     },
			    body: dataAsString
			};

			return request(options);
		}

		function parseInfoForLocations(info)
	 	{
	 		// console.log('info: ' + util.inspect(info));
		    var text = info;

		    var textCleaned = JSON.stringify(text);
		    textCleaned = textCleaned.replace(/\[\,/g, "[");
		    textCleaned = textCleaned.replace(/\\/g, "");
		    textCleaned = textCleaned.replace(/\"\{/g, "{");
		    textCleaned = textCleaned.replace(/\}\"/g, "}");

		    // console.log("Text: " + textCleaned);
		    var textParsed = JSON.parse(textCleaned);
		    let airportsInfoRaw = textParsed['1'][0]['2']['1'];

		    if (airportsInfoRaw == null)
		    {
		    	return [];
		    }
		    else
		    {
			    let airportsInfo = airportsInfoRaw.map(
			    	function(info)
			    	{
			    		return {
			    			airportCode : info['1'],
			    			airportName : info['2'],
			    			city : info['3'],
			    			regionAirportCode : info['4'],
			    			country : info['5'],
			    			longitude : info['6'],
			    			latitude : info['7'],
			    			provinceCode : info['8'],
			    			countryCode : info['9']
			    		}
			    	});

				let airportRegionsInfo = textParsed['1'][0]['2']['2'].map(
			    	function(info)
			    	{
			    		return {
			    			airportCode : info['1'],
			    			airportName : info['2'],
			    			longitude : info['3'],
			    			latitude : info['4'],
			    			childAirportCodes : info['5'],
			    			someAirportCodes : info['6'],
			    			country : info['7'],
			    			provinceCode : info['8'],
			    			countryCode : info['9'],
			    			province : info['10'],
			    		}
			    	});

			    var airportsInfoDictionary = airportsInfo.reduce(
			    	function(prev, airportInfo)
			    	{
			    		prev[airportInfo.airportCode] = airportInfo;
			    		return prev;
			    	}, {});

			    airportsInfoDictionary = airportRegionsInfo.reduce(
			    	function(prev, airportInfo)
			    	{
			    		prev[airportInfo.airportCode] = airportInfo;
			    		return prev;
			    	}, airportsInfoDictionary);		    

			    let airportRankings = textParsed['1'][0]['2']['3'].map(
			    	function(airportInfo)
			    	{		    		
			    		var regionCode = airportInfo['2'];
		    			var airportCode = airportInfo['1'] || regionCode;
			    		let result = {
					    			airportCode: airportCode,
					    			isAvailable: airportInfo['5']
					    		};
					    return result;
			    	});
			    
			    let airportsRanked = airportRankings.map(
			    	function(airportRanked)
			    	{
			    		return airportsInfoDictionary[airportRanked.airportCode];
			    	});

				// console.log('Text parsed: ' + JSON.stringify(airportsRanked, null, 2))
			    return airportsRanked;		    
			}
	  	}

		return getLocationsFromQueryFromGoogle(locationQuery)
			.then(
				function(result)
				{
					// console.log('result: ' + util.inspect(result));

					return parseInfoForLocations(result);
				})
	}

	static getTrips(startAirportCode, endAirportCode, minStartDate, maxStartDate, minDuration, maxDuration)
	{
	 	function parseInfoForFlights(info)
	 	{
	 		// console.log('info: ' + util.inspect(info));
		    var text = info;
		    // for (var key in info) {
		    //    // alert(' name=' + key + ' value=' + obj[key]);
		    //    console.log("Key: " + key);
		    // }

		    var textCleaned = JSON.stringify(text);
		    textCleaned = textCleaned.replace(/\[\,/g, "[");
		    textCleaned = textCleaned.replace(/\\/g, "");

		    console.log("Text: " + textCleaned);

		    //Fuck this messed up format. Let's use regex
		    var flightRegex = /"1":("[0-9]{4}-[0-9]{2}-[0-9]{2}"),"2":("[0-9]{4}-[0-9]{2}-[0-9]{2}"),"3":([0-9]+)/gi;
		    var foundStartDates = getMatches(textCleaned, flightRegex, 1);
		    var foundEndDates = getMatches(textCleaned, flightRegex, 2);
		    var foundPrices = getMatches(textCleaned, flightRegex, 3);

		    var flights = [];
			var now = new Date();
		    for (var flightIndex in foundStartDates)
		    {	    	
		        var foundStartDate = foundStartDates[flightIndex];
		        var foundEndDate = foundEndDates[flightIndex];
		        var foundPrice = foundPrices[flightIndex];        

		        var flight = new Object();
		        flight.startDate = foundStartDate;
		        flight.endDate = foundEndDate;
		        flight.price = parseInt(foundPrice);
		        flight.updateDate = now;

		        //TODO
		        //Get airline image url
		        //Get trip url
		        //Get start airport
		        //Get destination airport
		        //Get airlineImageUrl

				if (foundPrice > 0)
				{
					flights.push(flight);
				}
		    }

		    // console.log(JSON.stringify(flights, null, 2));
		    // for (var flightIndex in flights)
		    // {
		    //     var flight = flights[flightIndex];
		    //     console.log(flightIndex.toString() + ". Start: " + new Date(flight.startDate)   + " End: " + flight.endDate + " Price: " + flight.price.toString());
		    // }

		    return flights;
	  	}

		//Connect to Google and get flight data from it 
		function getInfoFromGoogle(startAirportCode, endAirportCode, startDate, endDate, durationInDays)
		{
			console.log("Getting info from Google...");
			//The payload that tells Google API what we info we want. Make sure the escapes are themselves escaped.
			var dataAsString = generateRequestPayload(startDate, endDate, startAirportCode, endAirportCode, durationInDays);
			// var dataAsString = '[,[[,"ca","[,{\\"1\\":[[,[\\"HKG\\"],[\\"KIX\\"],\\"\\"],[,[\\"KIX\\"],[\\"HKG\\"],\\"\\"]],\\"17\\":1,\\"18\\":[[,3,1]]},\\"2015-08-08\\",\\"2015-10-06\\",,[5]]","1877941884419597",9]],[,[[,"b_al","no:69"],[,"b_ahr","no:s"],[,"b_ca","193:13965"],[,"b_pe","51CD454107B21.AB6CA0A.2D059A39"],[,"b_qu","0"],[,"b_qc","1"]]]]'
			console.log("Generated payload: " + dataAsString.length.toString() + " bytes");

			//The options for the POST request
			//Note: Setting content-length or content-type cause a 500 response code to return
			var options = {
			    url: "https://www.google.ca/flights/rpc",
			    port: 443,
			    method: 'POST',
			    headers: { 
			        'x-gwt-permutation': x_gwt_permutation_default,
			     },
			    body: dataAsString
			};

			return request(options);
		}

	  // var generateRequestPayload = function(startAirportCode, endAirportCode, startDate, endDate)
	  // {
	  //   return '[,[[,"ca","[,{\\"1\\":[[,[\\"YYZ\\",\\"YTZ\\"],[\\"HKG\\"],\\"\\"],[,[\\"HKG\\"],[\\"YYZ\\",\\"YTZ\\"],\\"\\"]],\\"17\\":1,\\"18\\":[[,3,1]]},\\"2015-08-08\\",\\"2015-10-06\\",,[5]]","1877941884419597",9]],[,[[,"b_al","no:69"],[,"b_ahr","no:s"],[,"b_ca","193:13965"],[,"b_pe","51CD454107B21.AB6CA0A.2D059A39"],[,"b_qu","0"],[,"b_qc","1"]]]]';
	  // }

	    function generateRequestPayload(startDate, endDate, startAirportCode, endAirportCode, numberOfDays) {
	        //Convert dates to strings
	        var startDateString = startDate.toISOString().substring(0, 10);
	        var endDateString = endDate.toISOString().substring(0, 10);
	        // console.log("Start date string: " + startDateString);

	        //Start date must be in the future or Google will return nothing!
	        let json_start_location = '\\\"' + startAirportCode + '\\\"';       //need modifications splits string of airport codes with ABC,EFG,HIJ,... pattern
	                
	        let json = '[,[[,"ca","[,{\\"1\\":[[,[' + json_start_location+'],[\\"'+endAirportCode+'\\"],\\"\\"],[,[\\"'+endAirportCode+'\\"],['+json_start_location+'],\\"\\"]],\\"17\\":1,\\"18\\":[[,3,1]]},\\"'+startDateString+'\\",\\"'+endDateString+'\\",,[' + numberOfDays + ']]","1877941884419597",9]],[,[[,"b_al","no:69"],[,"b_ahr","no:s"],[,"b_ca","193:13965"],[,"b_pe","51CD454107B21.AB6CA0A.2D059A39"],[,"b_qu","0"],[,"b_qc","1"]]]]';
	             
	        return json;
	    }    
	        
	    var allFlights = [];
	    var totalPromises = [];

	    //Iterate over start date
	    // for (var startDate = new Date(minStartDate); startDate <= maxStartDate; startDate.setDate(startDate.getDate() + 1))
	    // {
	      //Iterate over duration
	      for (var duration = minDuration; duration <= maxDuration; duration++)
	      {
	        console.log(" Duration: " + duration);
	        console.log("About to get info from Google...");
	        var promise = getInfoFromGoogle(startAirportCode, endAirportCode, minStartDate, maxStartDate, duration).then(
	            function(result)
	            {
	                console.log("Successfully retrieved flight data from Google: " + JSON.stringify(result).length.toString() + ' bytes');// + JSON.stringify(result));
	                // console.log(JSON.stringify(result));

	                //Convert to a list of Flight objects
	                var flights = parseInfoForFlights(result);

	                allFlights = allFlights.concat(flights);
	                // response.success(flights);

	                // response.success(JSON.stringify(result));
	            }
	        );

	        totalPromises.push(promise);
	      }
	    // }

	    return Promise.all(totalPromises).then(
	        function(result)
	        {
	            //Sort by start date
	            allFlights.sort(function(a, b){
	              var dateAStart = new Date(a.startDate);
	              var dateBStart = new Date(b.startDate);
	              var dateAEnd = new Date(a.endDate);
	              var dateBEnd = new Date(b.endDate);

	              var dateStartDiff = dateAStart - dateBStart;
	              //If date start is same, check date end
	              if (dateStartDiff == 0)
	              {
	                return dateAEnd - dateBEnd;
	              }
	              else
	              {
	                return dateStartDiff;
	              }
	            })

	            // console.log(JSON.stringify(allFlights, null, 2));
	            return allFlights;
	        }
	    );
	}	
}

// test()
// function test()
// {
// 	GoogleFlightsApi.getPlacesFromName('hong kong')
// 		.then(
// 			function(places)
// 			{
// 				let topThree = places.slice(0,3);
// 				let topThreeNames = topThree.map(
// 					function(place)
// 					{
// 						return `${place.name}, ${place.countryName}`;
// 					});
// 				console.log('result: ' + JSON.stringify(topThreeNames, null, 2));
// 			});
// }

module.exports = GoogleFlightsApi;

function groupTripsByPrice(trips)
{
	let binnedInfo = trips.reduce(
		function(prev, trip)
		{
			var bin = Math.ceil(trip.price/10000)*10000;
			var binContents = prev[bin] || [];
			binContents.push(trip);

			prev[bin] = binContents;

			return prev;
		}, {});

	return binnedInfo;
}

// test()
function test()
{
	var startDate = new Date();
	var endDate = addDaysToDate(startDate, 7);
	GoogleFlightsApi.getTrips('YYZ', 'HKG', startDate, endDate, 4, 10 )
		.then(
			function(info)
			{
				// console.log('binnedInfo: ' + JSON.stringify(info));
				// let binnedInfo = groupTripsByPrice(info);
				// console.log('binnedInfo: ' + JSON.stringify(binnedInfo, null, 2));
				
				//Sort by price
				// info = info.sort(function(a, b) {
				//   return a.price - b.price;
				// });

				console.log('result: ' + JSON.stringify(info, null, 2));
			});
}

// test2();
function test2()
{
	GoogleFlightsApi.getPlacesFromName('12')
		.then(
				function(places)
				{
					console.log('result: ' + JSON.stringify(places, null, 2));
				});
}
