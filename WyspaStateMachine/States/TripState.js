"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');
const accounting = require('accounting');
const moment = require('moment');
const GenericMessenger = require('../../Messengers/GenericMessenger.js');

class TripState extends State {
	constructor()
	{
		super();
		this.name = 'trip';
	}


	onEnter(conversation, parameters)
	{
		return GoogleFlights.getTrips(
				parameters.origin.airportCode, 
				parameters.destination.airportCode,
				parameters.minStartDate, 
				parameters.maxStartDate, 
				parameters.minDuration, 
				parameters.maxDuration)
				.then(
					function(trips)
					{
						trips = trips.sort(function(a, b) {
						  return a.price - b.price;
						});

						//TODO Handle timezone
						var tripMessages = trips.map(
							function(trip)
							{
								let startDateFormatted = moment(trip.startDate).format('MMM DD (ddd), YYYY');
								let endDateFormatted = moment(trip.endDate).format('MMM DD (ddd), YYYY');
								let priceFormatted = accounting.formatMoney(trip.price/100);

								let webElement = GenericMessenger.createWebUrlElement('www.google.ca', 'Go to trip!');

								let testCardElement = GenericMessenger.createCardElement(`${startDateFormatted} to ${endDateFormatted}`, priceFormatted, null, [webElement]);
							});

						tripMessages = tripMessages.slice(0, 10);

						console.log('tripMessages: ' + util.inspect(tripMessages));

						return this.sendCards(conversation, tripMessages)
							.then(
								function()
								{
									return this.delegate.sendMessage(conversation, `Have a great trip!`);
								}.bind(this));
					}.bind(this));
	}

	processUser(conversation, message, parameters)
	{
		console.log('TripState: Processing...')
	}
}

module.exports = TripState;