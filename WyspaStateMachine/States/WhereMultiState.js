"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');

class WhereMultiState extends State {
	constructor()
	{
		super();
		this.name = 'where_multi';
	}


	onEnter(conversation, parameters)
	{
		let places = parameters.possibleDestinations;
		let topThree = places.slice(0,3);
		let topThreeNames = topThree.map(
			function(place)
			{
				return `${place.airportName}, ${place.country} (${place.airportCode})`;
			});
		let topThreeIds = topThree.map(
			function(place)
			{
				return place.airportCode;
			});

		return this.delegate.sendMessage(conversation, "Which one do you mean?", topThreeNames, topThreeIds);
	}

	processUser(conversation, message, parameters)
	{
		console.log('WhereMultiState: Processing...')
		
		let places = parameters.possibleDestinations;
		let topThree = places.slice(0,3);
		// let placeIndex = this.topThreeNames.indexOf(message.text);
		var matchingPlacesByPostback = topThree.filter(
			function( obj ) {
			  return obj.airportCode == message.text;
			});

		var matchingPlacesByIndex = topThree.filter(
			function( obj, index ) 
			{
			  return index.toString() == message.text;
			});

		var matchingPlace = matchingPlacesByPostback[0] || matchingPlacesByIndex[0];

		// console.log('matchingPlaces: ' + JSON.stringify(matchingPlaces));
		if (matchingPlace)
		{
			let place = matchingPlace;

			return this.delegate.sendMessage(conversation, `You've selected "${place.airportName}, ${place.country}"`)
				.then(
						function()
						{
							parameters.destination = place;
							return this.delegate.changeToState(conversation, 'when', parameters);
						}.bind(this));
		}
		else
		{
			parameters.destinationQuery = message;
			this.delegate.changeToState(conversation, 'where', parameters);
		}		
	}
}

module.exports = WhereMultiState;