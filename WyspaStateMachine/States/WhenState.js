"use strict";
const State = require('./State.js');

class WhenState extends State {
	constructor()
	{
		super();
		this.name = 'when';
		this.suggestions = ['This week', 'Next week', 'Later']
	}

  	onEnter(conversation, parameters)
	{
		return this.delegate.sendMessage(conversation, 'When do you want to leave?', this.suggestions);
	}

	processUser(conversation, message, parameters)
	{
		console.log('WhenState: Processing...')

		function addDaysToDate(date, days) {
		    var result = new Date(date);
		    result.setDate(result.getDate() + days);
		    return result;
		}

		var minStartDate = null;
		var maxStartDate = null;
		var today = new Date();

		if (message.text == this.suggestions[0])
		{
			minStartDate = new Date(today);
			maxStartDate = addDaysToDate(today, 6);
		}
		else if (message.text == this.suggestions[1])
		{
			minStartDate = addDaysToDate(today, 7);
			maxStartDate = addDaysToDate(today, 14);
		}
		else if (message.text == this.suggestions[2])
		{
			minStartDate = addDaysToDate(today, 15);
			maxStartDate = addDaysToDate(today, 120);
		}

		//TODO Do some NLP here, to handle a specific date ('next thursday'), range ('next week'), etc
		else if (false)
		{

		}
		else
		{
			return this.delegate.sendMessage(conversation, `Sorry. I didn't understand that."`)
				.then(
						function()
						{
							return this.restart();
						}.bind(this));
		}

		//Save parameters
		return this.delegate.sendMessage(conversation, `You've selected "${message.text}"`)
			.then(
					function()
					{
						parameters.minStartDate = minStartDate;
						parameters.maxStartDate = maxStartDate;
						return this.delegate.changeToState(conversation, 'duration', parameters);
					}.bind(this));
	}
}

module.exports = WhenState;