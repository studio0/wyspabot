"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');

class WhereState extends State {
	constructor()
	{
		super();
		this.name = 'where';
		this.suggestions = ['Not sure...'];
	}


	onEnter(conversation, parameters)
	{
		if (parameters.destinationQuery == null)
		{
			return this.delegate.sendMessage(conversation, `Where do you want to go?`, this.suggestions)	
		}
		else
		{
			return this.processUser(conversation, parameters.destinationQuery, parameters);
		}		
	}

	processUser(conversation, message, parameters)
	{		
		console.log('WhereState: Processing...')
		
		return this.delegate.sendMessage(conversation, `Ok ${message}, let me take a look...`)
			.then(
				function()
				{
					if (message == this.suggestions[0])
					{
						return this.delegate.sendMessage(conversation, `This feature is not ready yet!`)
							.then(
								function()
								{
									parameters.destinationQuery = null;
									return this.saveParameter(parameters)
										.then(
											function()
											{
												return this.restartState();
											})									
								}.bind(this));
					}
					else
					{
						//Process place name
						console.log('	>WhereState.processUser: Getting places...');
						return GoogleFlights.getPlacesFromName(message)
							.then(
								function(places)
								{				
									console.log('	>WhereState.processUser: Got places: ' + places.length);
									if (places.length > 1)
									{							
										parameters.possibleDestinations = places;
										return this.delegate.changeToState(conversation, 'where_multi', parameters);
									}
									else if (places.length == 1)
									{
										parameters.destination = places[0];
										let placeName = `${parameters.destination.airportName}, ${parameters.destination.country}`;
										return this.delegate.sendMessage(conversation, `Ok you're travelling to '${placeName}'`)
											.then(
												function()
												{
													this.delegate.changeToState(conversation, 'when', parameters);
												}.bind(this));
									}
									else
									{
										return this.delegate.sendMessage(conversation, "Sorry, I don't know any place with that name.")
											.then(
												function()
												{
													//Remove past location
													parameters.destinationQuery = null;
													return this.delegate.changeToState(conversation, 'where', parameters);
												}.bind(this));
									}
								}.bind(this));			
					}
				}.bind(this));
	}
}

module.exports = WhereState;