"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');

class OriginState extends State {
	constructor()
	{
		super();
		this.name = 'origin';
	}


	onEnter(conversation, parameters)
	{
		if (parameters.originQuery == null)
		{
			return this.delegate.sendMessage(conversation, `Where are you travelling from?`);
		}
		else
		{
			return this.processUser(conversation.user, parameters.originQuery, parameters);
		}		
	}

	processUser(conversation, message, parameters)
	{		
		console.log('OriginState: Processing...')
		
		return this.delegate.sendMessage(conversation, `Ok ${message}, let me take a look...`)
			.then(
				function()
				{
					
					//Process place name
					console.log('	>OriginState.processUser: Getting places...');
					return GoogleFlights.getPlacesFromName(message)
						.then(
							function(places)
							{				
								console.log('	>OriginState.processUser: Got places: ' + places.length);
								if (places.length > 1)
								{							
									parameters.possibleOrigins = places;
									return this.delegate.changeToState(conversation, 'origin_multi', parameters);
								}
								else if (places.length == 1)
								{
									parameters.origin = places[0];
									let placeName = `${parameters.origin.airportName}, ${parameters.origin.country}`;
									return this.delegate.sendMessage(conversation, `Ok you're travelling from '${placeName}'`)
										.then(
											function()
											{
												return this.delegate.changeToState(conversation, 'where', parameters);
											}.bind(this));
								}
								else
								{
									return this.delegate.sendMessage(conversation, "Sorry, I don't know any place with that name.")
										.then(
											function()
											{
												//Remove past location
												parameters.originQuery = null;
												return this.delegate.changeToState(conversation, 'origin', parameters);
											}.bind(this));
								}
							}.bind(this));			
				}.bind(this))
	}
}

module.exports = OriginState;