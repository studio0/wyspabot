"use strict";

const State = class {
  constructor()
  {
  	this.name = 'DefaultState';
    this.parameters = {};
  }

  onEnter(conversation, parameters)
  {
  	// console.log('onEnter');
  	return Promise.resolve();
  }

  onExit(conversation)
  {
  	// console.log('onExit');
  	return Promise.resolve();
  }

  processUser(conversation, message)
  {
  	var errorMessage = 'Subclass must override processUser';
  	console.error(errorMessage);
  	return Promise.reject(errorMessage);
  }
    
  // a and b are javascript Date objects
  dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  dateDiffInHours(a, b)
  { 
    var hours = Math.abs(b - a) / 36e5;
    return hours;
  }
};

module.exports = State;