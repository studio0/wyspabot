"use strict";
const State = require('./State.js');
const util = require('util');

class StartState extends State {
	constructor()
	{
		super();
		this.name = 'start';
	}


	onEnter(conversation, parameters)
	{
		console.log('StartState.onEnter');
		const fullName = conversation.user.get('fullName');
		return this.delegate.sendMessage(conversation, `Nice to meet you, ${fullName}`)
			.then(
				function()
				{
					return this.delegate.changeToState(conversation, 'origin');
				}.bind(this));
	}

	processUser(conversation, message, parameters)
	{
		console.log('StartState: Processing...')
		
		return this.delegate.changeToState(conversation, 'origin');
		// if (kikMessage.body == 'next')
		// {
		// 	return this.delegate.changeToState(user, this, 'when');
		// }
		// else
		// {
		// 	return kikMessage.addTextResponse('next').reply('What do you mean?');
		// }
	}
}

module.exports = StartState;