"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');

class DurationState extends State {
	constructor()
	{
		super();
		this.name = 'duration';
		this.suggestions = ['Less than week', 'Less than two weeks', 'Longer'];
	}


	onEnter(conversation, parameters)
	{
		return this.delegate.sendMessage(conversation, `How long are you staying? Type or choose an answer...`, this.suggestions)
	}

	processUser(conversation, message, parameters)
	{		
		console.log('DurationState: Processing...')

		function addDaysToDate(date, days) {
		    var result = new Date(date);
		    result.setDate(result.getDate() + days);
		    return result;
		}

		let startDate = parameters.startDate;

		var minDuration = null;
		var maxDuration = null;
		var today = new Date();

		if (message.text == this.suggestions[0])
		{
			minDuration = 0;
			maxDuration = 6;
		}
		else if (message.text == this.suggestions[1])
		{
			minDuration = 7;
			maxDuration = 13;
		}
		else if (message.text == this.suggestions[2])
		{
			minDuration = 14;
			maxDuration = 120;
		}
		//TODO Do some NLP here
		else if (false)
		{

		}
		else
		{
			return this.delegate.sendMessage(conversation, `Sorry. I didn't understand that."`)
				.then(
						function()
						{
							return stateMachine.restart();
						}.bind(this));
		}

		//Save parameters
		return this.delegate.sendMessage(conversation, `You've selected "${message.text}"`)
			.then(
					function()
					{
						parameters.minDuration = minDuration;
						parameters.maxDuration = maxDuration;
						return this.delegate.changeToState(conversation, 'trip', parameters);
					}.bind(this));
	}

	onExit(conversation)
	{
		//TODO Confirm details
		return this.delegate.sendMessage(conversation, 'Great! Let me look for some swag trips for you...');
	}
}

module.exports = DurationState;