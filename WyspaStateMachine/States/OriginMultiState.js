"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');

class OriginMultiState extends State {
	constructor()
	{
		super();
		this.name = 'origin_multi';
	}


	onEnter(conversation, parameters)
	{
		let places = parameters.possibleOrigins;
		let topThree = places.slice(0,3);
		let topThreeNames = topThree.map(
			function(place, index)
			{
				return `${index}. ${place.airportName}, ${place.country} (${place.airportCode})`;
			});
		let topThreeIds = topThree.map(
			function(place)
			{
				return place.airportCode;
			});

		return this.delegate.sendMessage(conversation, "Which one do you mean?", topThreeNames, topThreeIds);
	}

	processUser(conversation, message, parameters)
	{
		console.log('OriginMultiState: Processing...')
		// console.log('message: ' + util.inspect(message));
		
		let places = parameters.possibleOrigins;
		let topThree = places.slice(0,3);
		// let placeIndex = this.topThreeNames.indexOf(message.text);
		var matchingPlacesByPostback = topThree.filter(
			function( obj ) {
			  return obj.airportCode == message.text;
			});

		var matchingPlacesByIndex = topThree.filter(
			function( obj, index ) 
			{
			  return index.toString() == message.text;
			});

		var matchingPlace = matchingPlacesByPostback[0] || matchingPlacesByIndex[0];

		// console.log('matchingPlaces: ' + JSON.stringify(matchingPlaces));
		if (matchingPlace > 0)
		{
			let place = matchingPlace;

			return this.delegate.sendMessage(conversation, `You've selected "${place.airportName}, ${place.country}"`)
				.then(
					function()
					{
						parameters.origin = place;
						return this.delegate.changeToState(conversation, 'where', parameters);
					}.bind(this));
		}
		else
		{
			parameters.originQuery = message;
			this.delegate.changeToState(conversation, 'origin', parameters);
		}		
	}
}

module.exports = OriginMultiState;