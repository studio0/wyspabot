"use strict";
const State = require('./State.js');
const GoogleFlights = require('../../GoogleFlightsApi.js');
const util = require('util');

class BudgetState extends State {
	constructor()
	{
		super();
		this.name = 'budget';
		this.suggestions = ['$500', '$1000', 'above $1000'];
	}


	onEnter(conversation, parameters)
	{
		return this.delegate.sendMessage(`What is your budget?`, conversation.user.get('username'), this.suggestions)
	}

	processUser(conversation, message, parameters)
	{		
		const stateMachine = this;
		console.log('BudgetState: Processing...')
		
		// return this.delegate.sendMessage("Ok, let's see what I can find...", user.get('username'))
		// 	.then(
		// 		function()
		// 		{	
		var max = null;

		if (message.text == stateMachine.suggestions[0])
		{
			max = 500;
		}
		else if (message.text == stateMachine.suggestions[1])
		{
			max = 1000;
		}
		else if (message.text == stateMachine.suggestions[2])
		{
			max = null;
		}
		else
		{
			return stateMachine.delegate.sendMessage(`Sorry. I didn't understand that."`, conversation.user.get('username'))
				.then(
						function()
						{
							return stateMachine.delegate.restart(conversation);
						});
		}

		//Save parameters
		return stateMachine.delegate.sendMessage(`You've selected "${message.text}"`, conversation.user.get('username'))
			.then(
					function()
					{
						parameters.budgetMax = max;
						return stateMachine.delegate.changeToState(conversation, 'when', parameters);
					});
	}
}

module.exports = BudgetState;