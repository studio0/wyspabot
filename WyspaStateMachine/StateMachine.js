"use strict";
var State = require('./States/State.js');
var util = require('util');

class StateMachine
{
	constructor(statesArray) {	    
	    console.log('StateMachine: Initialized with ' + statesArray.length + ' states');

	    this.states = {};
	    this.initialStateName = null;

	    statesArray.forEach( 
	    	function(state) {
	    		//Set the delegate
	    		state.delegate = this;

	    		//Cache in the state dictionary
	    		this.states[state.name] = state;

	    		//Use the first element as the initial state
	    		if (!this.initialStateName)
			    {
			    	this.initialStateName = state.name;
			    	console.log('StateMachine: Initial state name is "' + this.initialStateName + '"');
			    }

		    }.bind(this));	    
	  }

	processConversation(conversation, message)
	{				
		let user = conversation.user;
		console.log('	>StateMachine.processUser: ' + user.get('username'));

		//Get state
		return this.getStateFromConversation(conversation)
			.then(
				function(state)
				{		
					if (state)
					{
						console.log('		>StateMachine.processUser: State is "' + state.name + '"');
						return state.processUser(conversation, message, state.parameters);
					}
					else
					{
						console.log('		>StateMachine.processUser: No state found, switching to Start state');
						return this.changeToState(conversation, this.initialStateName, {});
					}
				}.bind(this));	
	}

	restartState(conversation)
	{
		return this.getStateFromConversation(conversation)
			.then(
				function(state)
				{
					return this.changeToState(conversation, state.name);
				}.bind(this));
	}

	changeToState(conversation, targetStateName, parameters)
	{		
		let user = conversation.user;
		return this.getStateFromConversation(conversation)
			.then(
				function(currentState)
				{
					if (currentState == null)
					{
						currentState = new State();
					}

					console.log(util.format('		>changeToState: Changing from %s to %s for "%s"', currentState.name, targetStateName, user.get('username')));		

					//Update parameters if provided
					var newParameters = parameters;

					if (newParameters === undefined)
					{
						newParameters = currentState.parameters;
					}

					//Perform exit
					return currentState.onExit(conversation, newParameters)
						.then(
							function()
							{
								//Change State
								var stateAsJSON = {
									name: targetStateName,
									dateEntered: new Date(),
									parameters: newParameters
								}

								// user.set('State', StateAsJSON);
								console.log(util.format('			>changeToState: Saving state to user'));
								return this.saveConversationState(conversation, stateAsJSON)
									.then(
										function()
										{
											console.log(util.format('			>changeToState: Saved to user'));
											//Get target state
											var targetState = this.states[targetStateName];

											if (targetState == null)
											{
												throw new Error(`No state with name '${targetStateName}' exists!`);
											}
											else
											{
												//Perform enter
												return targetState.onEnter(conversation, currentState.parameters);
											}
										}.bind(this));
							}.bind(this));
				}.bind(this));
	}

	saveParameter(conversation, parameter)
	{
		var state = this.getStateFromConversation(conversation);
		state.parameters = parameter;

		return this.saveConversationState(conversation, state);
	}

	saveConversationState(conversation, state)
	{
		var conversationStates = conversation.user.get('conversations');

		if (conversationStates == null)
		{
			conversationStates = {};
		}

		conversationStates[conversation.type] = state;
		conversation.user.set('conversations', conversationStates);

		return conversation.user.save({ useMasterKey: true });
	}

	getStateFromConversation(conversation)
	{
		console.log('getStateFromConversation: Start')
		var user = conversation.user;
		return new Promise(
			function(resolve, reject)
			{
				//Get state
				var conversationStates = user.get('conversations');
				if (conversationStates != null)
				{
					var currentStateAsJSON = conversationStates[conversation.type];
				
					// console.log('getStateFromConversation.currentStateAsJSON: ' + JSON.stringify(currentStateAsJSON));

					if (currentStateAsJSON != null)
					{
						var state = this.states[currentStateAsJSON.name];
						if (state != null)
						{			
							state.parameters = currentStateAsJSON.parameters;
							state.dateEntered = currentStateAsJSON.dateEntered;
							resolve(state);
						}
						else
						{				
							reject('No state found for key: ' + currentStateAsJSON.name);
						}
					}
				}

				resolve();

			}.bind(this));
	}
}

module.exports = StateMachine;