"use strict";
const GenericMessenger = require('../Messengers/GenericMessenger.js');

const StartState = require('./States/StartState.js');
const WhereState = require('./States/WhereState.js');
const WhereMultiState = require('./States/WhereMultiState.js');
const BudgetState = require('./States/BudgetState.js');
const WhenState = require('./States/WhenState.js');
const DurationState = require('./States/DurationState.js');
const TripState = require('./States/TripState.js');
const OriginState = require('./States/OriginState.js');
const OriginMultiState = require('./States/OriginMultiState.js');
// const TripsState = require('./States/TripState.js');

const StateMachine = require('./StateMachine.js');

const util = require('util');

const Parse = require('parse/node');
// Parse.initialize(process.env.PARSE_APP_ID, process.env.PARSE_WEBHOOK_KEY, process.env.PARSE_MASTER_KEY);
Parse.initialize("VaY3ppBsi2tZQnVxFuoes438SlcYltHh7J78E9SY", "6eZ5Pgw3sAIrpaRDNugqJ6MBX2BbsjOq19JhDyZ4", "UC99voMsJkQ7FjoPQxMosSsUb7Zm50PE5V21QYo6");

// Parse.initialize("wypsaBot", "jsKey", "masterKey");
// Parse.serverURL = 'http://localhost:1337/parse'

const kDefaultPassword = 'Test1234!';

let MessageType = 
{
	Facebook : 'Facebook',
	Kik : 'Kik',
	Test : 'Test'
}

class WypsaStateMachine extends StateMachine {
	constructor()
	{
		//Get states
		const startState = new StartState();
		const whereState = new WhereState();
		const whereMultiState = new WhereMultiState();
		const budgetState = new BudgetState();
		const whenState = new WhenState();
		const durationState = new DurationState();
		const tripState = new TripState();
		const originState = new OriginState();
		const originMultiState = new OriginMultiState();
		// const tripsState = new TripsState();
		// var states = [startState, signupState, budgetState, whenState, durationState, tripsState];		
		var states = [startState, whereState, whereMultiState, whenState, budgetState, durationState, tripState, originState, originMultiState];
		super(states);

		//Messengers		
		this.messengers = {};
		// let fbMessenger = new FacebookMessenger()
		//fbMessenger, kikMessenger, testMessenger];
	}

	addMessenger(messenger)
	{
		return new Promise(
			function(resolve, reject)
			{
				this.messengers[messenger.name] = messenger;
				// console.log('this.messengers: ' + util.inspect(this.messengers));
				resolve();	
			}.bind(this));
	}

	processGenericMessage(text, id, type)
	{
		let stateMachine = this;

		console.log('WyspaStateMachine.processMessage: Received message from "' + id + '"');
		return this.getUser(id, type)
			.then(
				function(user)
				{
					let conversation = {
						user: user,
						type: type
					}

					return this.processConversation(conversation, text);
				}.bind(this));
	}

	getUser(userId, type)
	{
		let stateMachine = this;

		//Get existing Parse user
		Parse.Cloud.useMasterKey();
		var query = new Parse.Query(Parse.User);

		switch (type)
		{
			case MessageType.Facebook:
				query.equalTo('facebookId', userId);
				break;
			case MessageType.Kik:
				query.equalTo('kikId', userId);
				break;
			case MessageType.Test:
				query.equalTo('testId', userId);
				break;
		}		

		console.log('	>WyspaStateMachine.processMessage: Getting user...');
		return query.first({ useMasterKey: true })
			.then(
				function(parseUser)
				{
					console.log('		>WyspaStateMachine.processMessage: Got user');
					if (parseUser != null)
					{
						return parseUser;
					}
					else
					{						
						if (type == MessageType.Facebook)
						{
							if (stateMachine.fbBot)
							{								
								return stateMachine.fbBot.getUserProfile(userId)
							        .then(
							        	function(fbUser) 
							        	{
							        		let fullName = fbUser.first_name + ' ' + fbUser.last_name;
								            console.log(`WyspaStateMachine.processMessage: ${userId} (${fullName}) just registered!`);
											var user = new Parse.User();
											user.set("username", userId);
											user.set("password", kDefaultPassword);
											// user.set("email", kikUser.email);
											user.set("fullName", fullName);
											user.set('facebookId',  userId);
											user.set('conversations',  {});

											console.log(' >WyspaStateMachine.processMessage: Saving new user!')
											return user.signUp();
								        }
							        );
							}
							else
							{
								return Promise.reject('WyspaStateMachine.processMessage: No kik bot configured!');
							}
						}
						else if (type == MessageType.Kik)
						{
							if (stateMachine.kikBot)
							{
								return stateMachine.kikBot.getUserProfile(userId)
							        .then(
							        	function(kikUser) 
							        	{
								            console.log('WyspaStateMachine.processMessage: ' + userId + ' just registered!');
											var user = new Parse.User();
											user.set("username", userId);
											user.set("password", kDefaultPassword);
											// user.set("email", kikUser.email);
											user.set("fullName", kikUser.lastName + ' ' + kikUser.firstName);
											user.set('kikId',  userId);
											user.set('conversations',  {});

											console.log(' >WyspaStateMachine.processMessage: Saving new user!')
											return user.signUp();
								        }
							        );	
						    }
						    else
						    {
						    	return Promise.reject('WyspaStateMachine.processMessage: No kik bot configured!');
						    }
						}
						else if (type == MessageType.Test)
						{
							if (stateMachine.testBot)
							{
								return stateMachine.testBot.getUserProfile(userId)
							        .then(
							        	function(testUser) 
							        	{
								            console.log('WyspaStateMachine.processMessage: ' + userId + ' just registered!');
											var user = new Parse.User();
											user.set("username", testUser.username);
											user.set("password", kDefaultPassword);
											// user.set("email", kikUser.email);
											user.set("fullName", testUser.fullName);
											user.set('testId',  userId);
											user.set('conversations',  {});

											console.log(' >WyspaStateMachine.processMessage: Saving new user!')
											return user.signUp();
								        }
							        );	
						    }
						    else
						    {
						    	return Promise.reject('WyspaStateMachine.processMessage: No test bot configured!');
						    }
						}
						else
						{
							return Promise.reject('WyspaStateMachine.processMessage: Unknown message type!');
						}									
					}					
				});
	}

	// processFacebookMessage(text, fbId)
	// {
	// 	console.log('processFacebookMessage: Start');


	// 	return this.processMessage(
	// 	{
	// 		user: user,
	// 		type: MessageType.Facebook
	// 	},
	// 	userId);
	// }

	// processKikMessage(kikMessage)
	// {		
	// 	return this.processMessage(
	// 	{			
	// 		user: kikMessage.from,
	// 		type: MessageType.Kik
	// 	},
	// 	kikMessage.body);
	// }

	getUserIdFromConversation(conversation)
	{
		return this.getUserId(conversation.user, conversation.type);
	}

	getUserId(user, type)
	{
		return new Promise(
			function(resolve, reject)
			{
				var id = null;
				switch (type)
				{
					case MessageType.Facebook:
						// query.equalTo('facebookId', id);
						id = user.get('facebookId');
						break;
					case MessageType.Kik:
						id = user.get('kikId')
						break;
				}
				resolve(id);
			});		
	}

	//Messengers
	getMessengerForType(type)
	{
		return new Promise(
			function(resolve, reject)
			{
				let messenger = this.messengers[type];
				if (messenger != null)
				{
					resolve(messenger);
				}
				else
				{
					reject(`No messenger for type ${conversation.type} found!`);
				}
			}.bind(this));
	}

	sendMessage(conversation, text, suggestions, suggestionPostbacks)
	{
		if (suggestions)
		{
			let buttons = suggestions.map(
				function(suggestion, index)
				{
					let postback = null;

					if (suggestionPostbacks != null)
					{
						postback = suggestionPostbacks[index];
					}					
					
					return GenericMessenger.createPostbackElement(suggestion, postback || suggestion);
				});			
			return this.sendButtonMessage(conversation, text, buttons);
		}
		else 
		{
			return this.sendTextMessage(conversation, text);
		}
	}

	sendTextMessage(conversation, text) 
	{
		return this.getMessengerForType(conversation.type)
			.then(
				function(messenger)
				{
					return this.getUserIdFromConversation(conversation)
						.then(
							function(userId)
							{
								return messenger.sendTextMessage(userId, text);
							});					
				}.bind(this));
	}	

	sendButtonMessage(conversation, title, buttons) 
	{
		return this.getMessengerForType(conversation.type)
			.then(
				function(messenger)
				{
					return this.getUserIdFromConversation(conversation)
						.then(
							function(userId)
							{
								return messenger.sendButtonMessage(userId, title, buttons);
							});					
				}.bind(this));
	}

	sendCards(conversation, elements)
	{
		return this.getMessengerForType(conversation.type)
			.then(
				function(messenger)
				{
					return this.getUserIdFromConversation(conversation)
						.then(
							function(userId)
							{
								return messenger.sendCards(userId, elements);
							});					
				}.bind(this));
	}

	sendCardMessages(conversation, cardMessages)
	{	
		return this.getMessengerForType(conversation.type)
			.then(
				function(messenger)
				{
					return this.getUserIdFromConversation(conversation)
						.then(
							function(userId)
							{
								return messenger.sendCards(userId, cardMessages);
							});					
				}.bind(this));
	}
}

// var testBot = new WypsaStateMachine();
module.exports = WypsaStateMachine;