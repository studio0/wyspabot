unirest = require('unirest');

var request = unirest
.post('https://api.kik.com/v1/config')
.headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
.send({
        "webhook": "https://wyspabot.herokuapp.com/",
        "features": {
            "manuallySendReadReceipts": false,
            "receiveReadReceipts": false,
            "receiveDeliveryReceipts": false,
            "receiveIsTyping": false
        }
    })
.end(function (response) {
  console.log(response.body);
});

request.auth('pianobot', 'c71969e5-1f9c-44c4-a85f-924edc874365', true);