"use strict";
let kSabreId = 'V1:hqtokzm75c66yr29:DEVCENTER:EXT';
let kSabreSecret = '46vUUugV';

var SabreDevStudioFlight = require('sabre-dev-studio/lib/sabre-dev-studio-flight');
var sabre_dev_studio_flight = new SabreDevStudioFlight({
  client_id:     kSabreId,
  client_secret: kSabreSecret,
  uri:           'https://api.test.sabre.com'
});

var SabreDevStudio = require('sabre-dev-studio');
var sabre_dev_studio = new SabreDevStudio({
  client_id:     kSabreId,
  client_secret: kSabreSecret,
  uri:           'https://api.test.sabre.com'
});

// sabre_dev_studio_flight.theme_airport_lookup('BEACH', callback);
class SabreFlightsApi {
	constructor()
	{
	}
	
	static getThemes()
	{
		return new Promise(
			function(resolve, reject)
			{
				sabre_dev_studio_flight.travel_theme_lookup(
					function(error, data) {
					  if (error) {
					    // Your error handling here
					    reject(error);
					  } else {
					    // Your success handling here
					    resolve(JSON.parse(data));
					  }
					});
			});
	}

	static getThemes()
	{
		return new Promise(
			function(resolve, reject)
			{
				sabre_dev_studio_flight.travel_theme_lookup(
					function(error, data) {
					  if (error) {
					    // Your error handling here
					    reject(error);
					  } else {
					    // Your success handling here
					    resolve(JSON.parse(data));
					  }
					});
			});
	}

	static getPlacesFromName(placeName)
	{
		// TODO
		// return Promise.resolve(['Toronto', 'Hong Kong', 'Tokyo']);
		let query = 
		{
			query: encodeURIComponent(placeName),
			category: 'CITY'
		}

		return new Promise(
			function(resolve, reject)
			{
				sabre_dev_studio.get('/v1/lists/utilities/geoservices/autocomplete', query,
					function(error, data) {
					  if (error) {
					    // Your error handling here
					    reject(error);
					  } else {
					    // Your success handling here		    
					    let responseParsed = JSON.parse(data);
					    let grouped = responseParsed['Response']['grouped'];
					    resolve(grouped != null ? grouped['category:CITY']['doclist']['docs'] : []);
					  }
					});
			});
	}

	static validatePlace(place)
	{
		// TODO
		return Promise.resolve(true);
	}
}

// test()
function test()
{
	SabreFlightsApi.getPlacesFromName('hong kong')
		.then(
			function(places)
			{
				let topThree = places.slice(0,3);
				let topThreeNames = topThree.map(
					function(place)
					{
						return `${place.name}, ${place.countryName}`;
					});
				console.log('result: ' + JSON.stringify(topThreeNames, null, 2));
			});
}

module.exports = SabreFlightsApi;