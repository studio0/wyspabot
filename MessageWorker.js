'use strict';

//Facebook
const util = require('util');

//StateMachine
let WyspaStateMachine = require('./WyspaStateMachine/WyspaStateMachine.js')
let wyspaStateMachine = new WyspaStateMachine();

//Facebook bot
var FacebookMessenger = require('./Messengers/FacebookMessenger.js')
const kFBMessengerToken = "CAAMCrxv6CFIBAH6fevBtnyp4dHK0sEUwoc44IGzZBDDK6RNQlCTvqrQnTtUsZBAttPAAjerQxbTl8upcXWkWyFiYbtOzhDsEjnGdZBLA5aZBX23NSsiYyrY7BkzZBEMeH0Gmud7WU3xIJ92Nt48IIoNkj7xfDsYJ70Pb57qrbdJ7Eyb80ySwPpviZB3tpXGXMZD";
let fbBot = new FacebookMessenger(kFBMessengerToken);
wyspaStateMachine.addMessenger(fbBot);

//Test bot
var TestMessenger = require('./Messengers/TestMessenger.js')
let testBot = new TestMessenger();
wyspaStateMachine.addMessenger(testBot);

var kue = require('kue');
var redis = require('redis');

//redis
var queue = kue.createQueue({
  // redis: process.env.REDIS_URL
	redis: "redis://h:pekptr94ndogqk2qq3ao9qbprq0@ec2-54-227-251-101.compute-1.amazonaws.com:9389"//process.env.REDIS_URL
});

queue.watchStuckJobs();

queue.process('message', function(job, done) {
  // console.log('MessageWorker.message: Message received: ' + util.inspect(job.data));
  return processEvent(job.data)
  	.then(
  		function()
  		{
  			console.log('	>MessageWorker.message: Done!');
  			done();
  		})
  	.catch(
  		function(err)
  		{  			
  			console.error(err);
  			done();
  		})
});

function processEvent(event)
  {
  	console.log('>MessageWorker.processEvent: Start...');

    let senderId = event.senderId;
    // let postback = event.postback;
    let text = event.text;
    let type = event.type;

  	console.log(`Got message: ${text} from ${senderId} of type '${type}'`);
  	return wyspaStateMachine.processGenericMessage(text, senderId, type);
  }

