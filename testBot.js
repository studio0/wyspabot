"use strict";

var request = require('request-promise');

return request({
        url: 'http://127.0.0.1:4444/webhook',
        method: 'POST',
        json: {
          entry: [
          {
            messaging: [
            {
              sender:{
                id: 1234
              },
              message :{
                text: "hi"
              }
            }]
          }]
        }
      }, function(error, response, body) {
        if (error) {
          console.log('Error sending message: ', error);
        } else if (response.body.error) {
          console.log('Error: ', response.body.error);
        }
      });