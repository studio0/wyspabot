'use strict';

var util = require('util');
var express = require('express');
var bodyParser = require('body-parser')
var kue = require('kue');
var redis = require('redis');

//redis
var queue = kue.createQueue({
  redis: process.env.REDIS_URL
});

//FB
const kValidationToken = 'wyspaIsAwesome!';

//Server
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//Facebook verify webhook
app.get('/webhook/', function (req, res) {
  if (req.query['hub.verify_token'] === kValidationToken) {
    res.send(req.query['hub.challenge']);
  }
  res.send('Error, wrong validation token');
})

//Facebook message received webhook
app.post('/webhook/', function (req, res) {  
	// console.log('req: ' + util.inspect(req.body));
  	let messaging_events = req.body.entry[0].messaging;
  
  	console.log('>Got events: ' +  messaging_events.length);

  	messaging_events.forEach(
		function(event, index)
		{
			console.log('	>Processing event: ' +  index);
		    let senderId = event.sender.id.toString();
		    let postback = event.postback != null ? event.postback.payload : null;
		    let text = event.message != null ? event.message.text : postback;

		    if (text)
		    {
			    let event = 
			    {
			    	senderId : senderId,
			    	text : text,
			    	type : 'Facebook'
			    }
		        // let type = event.type.toString();

			    // console.log('	>Got event: ' + util.inspect(event));
		      	var job = queue.create('message', event)
				    .save((err) => {
				      if (err) {
				        console.error(err);
				      }
				      if (!err) {
				      	console.log('	>Saved!');
				      }
				    });
			}
			else
			{
		      	console.log('	>Skipped non-text event!');
			}
		});

	  	res.sendStatus(200);
	  	console.log('	>Saved!');
});

app.set('port', process.env.PORT || 4444);
var server = app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + server.address().port);
});